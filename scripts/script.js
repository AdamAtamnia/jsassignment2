//Adam Atamnia 2036819

'use strict';
/** 
 * JS Assignment 2
 *  Astronomy Picture of the Day
 * 
 * @author Adam Atamnia
 * 2021-11-8
 */
document.addEventListener('DOMContentLoaded', setup)

let nasaDataMap = new Map();

/**
 * First funtion that is called after the event (DOMContentLoaded)
 * @author Adam Atamnia
 */
function setup() {
    let form = document.querySelector('form')
    form.addEventListener('submit', submit)
}

/**
 * Retrieves a JSON object form a NASA api and calls addToMap(nasaData)
 * @author Adam Atamnia
 */
function submit(e){
    e.preventDefault();
    fetch("https://api.nasa.gov/planetary/apod?count=1&api_key=gDUz7nREIaZ3ivAw5V6vaykGTgAJe2lD6EY1nd2S")
    .then(responce => {
        if (responce.ok) {
            return responce.json()
        } else {
            badResponceAction()
            throw new Error("Responce Not 200")
        }
    }, () => badResponceAction())
    .then(nasaData => addToMap(nasaData[0]))
    .catch(() => badResponceAction())
}

/**
 * Adds an object to the global map nasaDataMap, calls updateMap(), addBtn(nasaData) and displayData(nasaData).
 * @param {object} nasaData NASA api object 
 * @author Adam Atamnia
 */
function addToMap(nasaData) {
    updateMap();
    if (!(nasaDataMap.has(nasaData.title))){
        nasaDataMap.set(nasaData.title, { title: nasaData.title, explanation: nasaData.explanation, media_type: nasaData.media_type, hdurl: nasaData.hdurl });
        addBtn(nasaData);
    }
    console.log(nasaDataMap);
    displayData(nasaData);
}

/**
 * Deletes the first object of the map and the first button from the html when the objects inserted are more then 5.
 * @author Adam Atamnia
 */
function updateMap() {
    if (nasaDataMap.size == 5) {
        nasaDataMap.delete(Array.from(nasaDataMap.keys())[0]);
        let btn = document.querySelector('.recentObjBtn');
        btn.remove();
    }
}

/**
 * Adds the explanation and image of the given object to the html
 * @param {object} nasaData NASA api object or object in global map
 * @author Adam Atamnia
 */
function displayData(nasaData){
    let para = document.querySelector('.textPara')
    para.style.color = "black"
    para.textContent = nasaData.explanation

    let img = document.querySelector(".photoImage")
    if (nasaData.media_type === "image"){
        img.src = nasaData.hdurl
    } else {
        img.src = "images/No_image_available.png"
    }
}

/**
 * Adds a button corresponding to the given object in the appropriate section of the html.
 * Adds an event listener to that btn to calling displayData(nasaData) on click.
 * @param {object} nasaData NASA api object
 * @author Adam Atamnia
 */
function addBtn(nasaData) {
    let btnSection = document.querySelector('.btnSection');
    let btn = document.createElement("button");
    btn.classList.add("recentObjBtn")
    btn.textContent = nasaData.title;
    btnSection.appendChild(btn);

    btn.addEventListener('click', () => {displayData(nasaDataMap.get(btn.textContent))})
}

/**
 * Indicates an error to the user.
 * @author Adam Atamnia
 */
function badResponceAction(){
    let para = document.querySelector('.textPara')
    para.style.color = "red"
    para.textContent = "Error, try again!"

    let img = document.querySelector(".photoImage")
    img.src = "images/No_image_available.png"

}
